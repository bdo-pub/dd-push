## [2.3.2](https://gitlab.com/to-be-continuous/defectdojo/compare/2.3.1...2.3.2) (2023-09-20)


### Bug Fixes

* error handling in case the product is not found ([27502ad](https://gitlab.com/to-be-continuous/defectdojo/commit/27502ad3873e2354cc8d8467bde397d17793429f))

## [2.3.1](https://gitlab.com/to-be-continuous/defectdojo/compare/2.3.0...2.3.1) (2023-09-19)


### Bug Fixes

* **curl:** only activate verbose logs when $TRACE is set ([163da55](https://gitlab.com/to-be-continuous/defectdojo/commit/163da55b2a95bd8f86041ab6f4b8b1a45163e995)), closes [#34](https://gitlab.com/to-be-continuous/defectdojo/issues/34)

# [2.3.0](https://gitlab.com/to-be-continuous/defectdojo/compare/2.2.0...2.3.0) (2023-09-14)


### Features

* add Zap reports support ([a7ed443](https://gitlab.com/to-be-continuous/defectdojo/commit/a7ed4435c50d4ea7b74d6c1031a6163053c3a634))

# [2.2.0](https://gitlab.com/to-be-continuous/defectdojo/compare/2.1.1...2.2.0) (2023-09-12)


### Features

* add vault variant ([3bb0167](https://gitlab.com/to-be-continuous/defectdojo/commit/3bb0167e2f1e27df00bb2b913babbdd0e681f64a))

## [2.1.1](https://gitlab.com/to-be-continuous/defectdojo/compare/2.1.0...2.1.1) (2023-08-04)


### Bug Fixes

* **gitleaks:** fix Gitleaks report path ([b7b478d](https://gitlab.com/to-be-continuous/defectdojo/commit/b7b478dde17e7de87a93e925a359dda79cc5707b))

# [2.1.0](https://gitlab.com/to-be-continuous/defectdojo/compare/2.0.7...2.1.0) (2023-05-27)


### Features

* **workflow:** extend (skip ci) feature ([8214bcb](https://gitlab.com/to-be-continuous/defectdojo/commit/8214bcbed0b3bac44781868096cf8a7404e79be4))

## [2.0.7](https://gitlab.com/to-be-continuous/defectdojo/compare/2.0.6...2.0.7) (2023-03-11)


### Bug Fixes

* Add TestSSL Scan ([34891f8](https://gitlab.com/to-be-continuous/defectdojo/commit/34891f81273f89475ba58d4796ddb9fcf3a7fb71))

## [2.0.6](https://gitlab.com/to-be-continuous/defectdojo/compare/2.0.5...2.0.6) (2023-03-07)


### Bug Fixes

* Resolve "Wrong if statement" ([e45b8dd](https://gitlab.com/to-be-continuous/defectdojo/commit/e45b8dd5f044d9e50bf0f971d9bb0ea0ea61f624))

## [2.0.5](https://gitlab.com/to-be-continuous/defectdojo/compare/2.0.4...2.0.5) (2023-03-07)


### Bug Fixes

* Resolve "Finding auto close stopped working" ([0441cec](https://gitlab.com/to-be-continuous/defectdojo/commit/0441cec2aee1c8a70635da7d22249bf4b0a99986))

## [2.0.4](https://gitlab.com/to-be-continuous/defectdojo/compare/2.0.3...2.0.4) (2023-01-27)


### Bug Fixes

* "Add registry name in all Docker images" ([23a6754](https://gitlab.com/to-be-continuous/defectdojo/commit/23a6754db334ec4973fb4445ba488d90fe5eb0cb))

## [2.0.3](https://gitlab.com/to-be-continuous/defectdojo/compare/2.0.2...2.0.3) (2022-12-13)


### Bug Fixes

* globbing not supported in report paths ([c2160c9](https://gitlab.com/to-be-continuous/defectdojo/commit/c2160c958e411061a8ad0e72197af20c123a0e92))

## [2.0.2](https://gitlab.com/to-be-continuous/defectdojo/compare/2.0.1...2.0.2) (2022-10-10)


### Bug Fixes

* use of $SONAR_AUTH_TOKEN ([d0fa5e8](https://gitlab.com/to-be-continuous/defectdojo/commit/d0fa5e80ef730b8548b0a64061b0bd123e32b4b5))
* use of $SONAR_AUTH_TOKEN ([16b69a2](https://gitlab.com/to-be-continuous/defectdojo/commit/16b69a2685cf1065640331ff6467d79fc53acf02))

## [2.0.1](https://gitlab.com/to-be-continuous/defectdojo/compare/2.0.0...2.0.1) (2022-10-10)


### Bug Fixes

* Adapt to report normalization ([5574950](https://gitlab.com/to-be-continuous/defectdojo/commit/557495027c509167e3aef65804a9e10d5146af39))

# [2.0.0](https://gitlab.com/to-be-continuous/defectdojo/compare/1.2.0...2.0.0) (2022-08-05)


### Bug Fixes

* use $CI_COMMIT_REF_NAME instead of $CI_COMMIT_BRANCH (branch pipeline ionly) ([ee00f93](https://gitlab.com/to-be-continuous/defectdojo/commit/ee00f9348c13f0e3ea42a98949c0b70f7966e7b4))


### Features

* make MR pipeline the default workflow ([ae27cb5](https://gitlab.com/to-be-continuous/defectdojo/commit/ae27cb506b9ba8a03a8b7a2771b88dd9b5b2a706))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

# [1.2.0](https://gitlab.com/to-be-continuous/defectdojo/compare/1.1.1...1.2.0) (2022-06-30)


### Bug Fixes

* add mail template ([ea32516](https://gitlab.com/to-be-continuous/defectdojo/commit/ea32516d01bb7b89aea421df8143c7a11a034870))
* call the import_into_defectdojo function from the job instead of the script ([a27f610](https://gitlab.com/to-be-continuous/defectdojo/commit/a27f610f527f9c2b841b3f84e15cf7fff7c4b340))
* check that DEFECTDOJO_SMTP_SERVER has a value ([a204325](https://gitlab.com/to-be-continuous/defectdojo/commit/a204325ccd1c4068d82b612c3975395889e72cf4))
* check that SMTP var has a value ([f7e436d](https://gitlab.com/to-be-continuous/defectdojo/commit/f7e436d04e9b10c29590436c458ddc95b11cd4e2))
* check that SMTP var is defined ([50d8f19](https://gitlab.com/to-be-continuous/defectdojo/commit/50d8f19c38daf830bd82b7e8b9db4bb2c54d7538))
* DEFECTDOJO_SMTP_SERVER documentation and DEFECTDOJO_NOTIFICATION removal ([b3fe8f4](https://gitlab.com/to-be-continuous/defectdojo/commit/b3fe8f4bdf313da41dc773a42c7d1330f0babee9))
* find Trivy reports that are not in the trivy directory ([5acf5f2](https://gitlab.com/to-be-continuous/defectdojo/commit/5acf5f27ce12eace1aa2d539fd92d51626a84fe3))
* **shellcheck:** simplify test ([8160409](https://gitlab.com/to-be-continuous/defectdojo/commit/8160409a086864a995737f9e335726123a578da1))
* update line starts ([ad8bf9b](https://gitlab.com/to-be-continuous/defectdojo/commit/ad8bf9b3020b80e8887dac5a75f496d0bc86af04))


### Features

* link each finding to the related component ([6eaa3a2](https://gitlab.com/to-be-continuous/defectdojo/commit/6eaa3a219dfae63dae2c4a3248b0e07750f98a13))

## [1.1.1](https://gitlab.com/to-be-continuous/defectdojo/compare/1.1.0...1.1.1) (2022-05-17)


### Bug Fixes

* remove SONAR_BRANCH_NAME ([f3bd1e6](https://gitlab.com/to-be-continuous/defectdojo/commit/f3bd1e6e4e75d0e50fc54b7ab21266a9a8ca0810))

# [1.1.0](https://gitlab.com/to-be-continuous/defectdojo/compare/1.0.4...1.1.0) (2022-05-01)


### Features

* configurable tracking image ([91af529](https://gitlab.com/to-be-continuous/defectdojo/commit/91af529f6de5c6cfae2768a0ce61ce19f3579c1a))

## [1.0.4](https://gitlab.com/to-be-continuous/defectdojo/compare/1.0.3...1.0.4) (2022-04-02)


### Bug Fixes

* **nodejsscan:** use Node template version to be consistent with other tools' imports ([42c2a7e](https://gitlab.com/to-be-continuous/defectdojo/commit/42c2a7e74851beaca4b6f63359e695025f6fd967))

## [1.0.3](https://git-us-east1-c.ci-gateway.int.gprd.gitlab.net:8989/to-be-continuous/defectdojo/compare/1.0.2...1.0.3) (2022-03-15)


### Bug Fixes

* exclude merge request pipelines ([cb112f3](https://git-us-east1-c.ci-gateway.int.gprd.gitlab.net:8989/to-be-continuous/defectdojo/commit/cb112f3bf7f731cf985c6c93a3919590350f11d7))

## [1.0.2](https://gitlab.com/to-be-continuous/defectdojo/compare/1.0.1...1.0.2) (2022-03-11)


### Bug Fixes

* remove incorrect test description update ([317fd87](https://gitlab.com/to-be-continuous/defectdojo/commit/317fd87ab048ba00125422389853e95019e941ad))
* support true and false values for DEFECTDOJO_NOPROD_ENABLED ([396d431](https://gitlab.com/to-be-continuous/defectdojo/commit/396d4313a2b1ce493ad9451f3eebf10836f6c304))

## [1.0.1](https://gitlab.com/to-be-continuous/defectdojo/compare/1.0.0...1.0.1) (2022-02-25)


### Bug Fixes

* **kicker:** add kind analyse to enable kicker ([cc1f68a](https://gitlab.com/to-be-continuous/defectdojo/commit/cc1f68af374f159e4bc07d41f93a72a34465a910))

# 1.0.0 (2022-02-25)


### Features

* initialize defectdojo template ([e4518e3](https://gitlab.com/to-be-continuous/defectdojo/commit/e4518e3120ba5928d702a140582c2c20fcda8f3f))
