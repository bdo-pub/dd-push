# GitLab CI template for DefectDojo

This project implements a GitLab CI/CD template for [DefectDojo](https://github.com/DefectDojo/django-DefectDojo) dashboard.

DefectDojo is a security program and vulnerability management tool that allows you to consolidate all the vulnerabilities of your project into one source of truth.

:warning: By default, DefectDojo will collect vulnerabilities only on your `master` or your `main` branch. For collecting vulnerabilities on another branch, set `PROD_REF` variable to your branch name.

Please read [DefectDojo documentation](https://defectdojo.github.io/django-DefectDojo/) for further information.

## Get started

### DefectDojo instance

You must have access to a DefectDojo server and set `DEFECTDOJO_SERVER_URL` variable according to this server's URL. This server must be configured to allow "Login with Gitlab" for the Gitlab which hosts your projects.

### Login with Gitlab, include template and define variables in your project

The initial mandatory step for a developer is to click on "Login with Gitlab" button on login page (see [dedicated section](#first-connection-to-defectdojo)) in order to retrieve his DefectDojo API v2 key.

All GitLab projects you are developer / maintainer / owner of will be automatically imported into DefectDojo dashboard. The name of the DefectDojo product will be set to the `CI_PROJECT_PATH` variable of the GitLab project.

Once you have your API v2 key, you may use it :

- by including the current template in your `.gitlab-ci.yml`. This is the recommended solution, and the reason why the current template exists (see [usage](#usage)). Don't forget to define DEFECTDOJO_ variables listed in [defectdojo job](#defectdojo-job). If needed, you might also update DEFECTDOJO_ variables in [Global variables](#global-variables).
- through DefectDojo API, making your own calls to the API

### Check you use at least one security tool

DefectDojo job will NOT launch security tools: it will only import security reports that have been generated on YOUR demand in your `.gitlab-ci.yml`. We strongly advise you to use [to be continuous templates](https://gitlab.com/to-be-continuous) to launch security tools.
Some security tools are already included in more generic templates (such as [python](https://gitlab.com/to-be-continuous/python)), but other security tools are the object of a specific template (such as [sonar](https://gitlab.com/to-be-continuous/sonar)).

So make sure that your `.gitlab-ci.yml` config launches at least one of the [supported security tools](#supported-security-tools), otherwise the current template will be useless.

### Every day usage in your pipelines

Every time your CI/CD pipeline is launched, if you have at least one security tool which generates a security report, then all of the newly generated security reports will be imported into DefectDojo, you have nothing to do.

You may connect to DefectDojo server to visualize the summary of your project's vulnerabilities. You might also receive an e-mail notification if your project contains new vulnerabilities, depending on the threshold you defined in `DEFECTDOJO_NOTIFICATION_SEVERITIES` and on the fact you defined `DEFECTDOJO_SMTP_SERVER`.

## Usage

In order to include this template in your project, add the following to your `.gitlab-ci.yml`:

```yaml
  - project: 'to-be-continuous/defectdojo'
    ref: '2.3.2'
    file: '/templates/gitlab-ci-defectdojo.yml'
```

## defectdojo job

This job uploads security reports found in the artifacts to a DefectDojo server.

It is bound to the `.post` stage, and uses the following variables:

| Name                          | description                            | default value     |
| ----------------------------- | -------------------------------------- | ----------------- |
| `DEFECTDOJO_SERVER_URL`       | URL of DefectDojo server               | _none_            |
| :lock: `DEFECTDOJO_API_KEY`   | Your DefectDojo API v2 Key (see [below](#first-connection-to-defectdojo) how to retrieve it)            | _none_            |

## Global variables

Depending on the security tools you launch, and their configuration, you may have to redefine some of the following variables in your `gitlab-ci.yml`:

| Name                                     | description                                                                                                                            | default value                             |
|------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------| ----------------------------------------- |
| `DEFECTDOJO_BANDIT_REPORTS`              | path to Bandit JSON reports                                                                                                            | `bandit*.json reports/py-bandit.bandit.json`                        |
| `DEFECTDOJO_DC_GRADLE_REPORTS`           | path to Dependency Check XML reports (Gradle template)                                                                                 | `dependency-check*.xml` |
| `DEFECTDOJO_DC_REPORTS`                  | path to Dependency Check XML reports (Maven template)                                                                                  | `dependency-check*.xml`        |
| `DEFECTDOJO_GITLEAKS_REPORTS`            | path to Gitleaks JSON reports                                                                                                          | `gitleaks/gitleaks-report.json reports/gitleaks.native.json` || `DEFECTDOJO_HADOLINT_REPORTS`            | path to Hadolint JSON reports                                                                                                          | `hadolint-json-*.json reports/docker-hadolint-*.native.json`             |
| `DEFECTDOJO_MOBSF_REPORTS`               | path to MobSF JSON reports                                                                                                             | `mobsf*.json`                        |
| `DEFECTDOJO_NODEJSSCAN_REPORTS`          | path to NodeJSScan SARIF reports                                                                                                       | `nodejsscan-report-sarif.json`                        |
| `DEFECTDOJO_NPMAUDIT_REPORTS`            | path to NPM Audit JSON reports                                                                                                         | `npm-audit*.json`                        |
| `DEFECTDOJO_TESTSSL_REPORTS`             | path to TestSSL CSV reports                                                                                                            | `reports/testssl.native.csv`                        |
| `DEFECTDOJO_ZAP_REPORTS`               | path to Zap xml reports                                                                                                             | `reports/zap.native.xml`            |
| `ZAP_TPL_PROJECT`        | path to Zap template (if any) | _none_ | 
| `DEFECTDOJO_TRIVY_REPORTS`               | path to Trivy JSON reports                                                                                                             | `trivy/*.json trivy-*.json reports/docker-trivy-*.native.json reports/py-trivy.trivy.json`             |
| `DEFECTDOJO_SONARQUBE_APPLICATION`       | your application name in Sonar                                                                                                         | ex: project-name                         |
| `DEFECTDOJO_SONARQUBE_PROJECT_KEY`       | your project key in Sonar (be careful, the project key might be distinct from project-dir-project-name, sometimes Sonar adds a suffix) | ex: project-dir-project-name or        project-dir-project-name__AXjLJ76cpGZrj3BUYg10                 |
| `DEFECTDOJO_SONARQUBE_SINCELEAKPERIOD`   | determines if delta analysis is activated                                                                                              | false                         |
| `DEFECTDOJO_SONARQUBE_NOSECURITYHOTSPOT` | if true, disable hotspots processing                                                                                                   | true                         |
| `DEFECTDOJO_SONARQUBE_ALLBUGS `          | true if all bugs are returned, false if only vulnerabilities are returned                                                              | false                         |
| `DEFECTDOJO_BASE_IMAGE`                  | Alpine base image                                                                                                                      | `registry.hub.docker.com/library/node:alpine3.11`                         |
| `DEFECTDOJO_SMTP_SERVER`                 | name:port of SMTP server used for notifications                                                                                        | dist                         |
| `DEFECTDOJO_NOTIFICATION_SEVERITIES`     | list of (comma separated) severities to be notified on                                                                                 | Critical,High                         |
| `DEFECTDOJO_TIMEZONE`                    | timezone used for naming engagements and setting engagements start and end                                                             | `Europe/Paris`                  |
| `DEFECTDOJO_NOPROD_ENABLED`              | determines if defectdojo job is launched on non production branches                                                                    | false                         |


## First connection to DefectDojo

Connect to the DefectDojo server and click on "Login with Gitlab". You will be redirected to the Gitlab login screen. Once logged in, you will be presented with a consent screen where you can accept to share your information with DefectDojo. DefectDojo will then automatically create a DefectDojo Product for each Gitlab project in which you have  either owner or maintainer role.

:warning: Synchronization between your Gitlab projects and your DefectDojo products occurs only each time you "login with Gitlab". Therefore, if you have a new Gitlab project, or if you leave a Gitlab project, you need to login to DefectDojo once again.

Once logged into DefectDojo for the first time, retrieve your API v2 key by clicking on the user logo in the top-right corner. Store this API key into a variable named `DEFECTDOJO_API_KEY`.


## Supported security tools

For now, the current template allows to import reports from the following tools:

- Bandit
- Dependency Check
- Gitleaks
- Hadolint
- MobSF
- NodeJSScan
- NPM Audit
- SonarQube 
- TestSSL
- Trivy
- Zap

## SonarQube specific case

SonarQube is specific: contrary to other jobs, sonar job does not generate any artifact, but just launches an analysis on SonarQube server. In `defectdojo` job, a tool named `sonar-report` generates an HTML report thanks to Sonar API ; this report is finally imported in DefectDojo.

In order to launch SonarQube HTML-report generation, the following variables must be set:

- SONAR_URL (used by sonar template)
- SONAR_TOKEN (used by sonar template)
- DEFECTDOJO_SONARQUBE_APPLICATION
- DEFECTDOJO_SONARQUBE_PROJECT_KEY

The following variables are set by default, but may be overriden:

- DEFECTDOJO_SONARQUBE_SINCELEAKPERIOD (default: false)
- DEFECTDOJO_SONARQUBE_NOSECURITYHOTSPOT (default: true)
- DEFECTDOJO_SONARQUBE_ALLBUGS (default: false)

The following command is executed:

```yaml
sonar-report \
    --sonarurl="${SONAR_URL}" \
    --sonarcomponent="${DEFECTDOJO_SONARQUBE_PROJECT_KEY}" \
    --project="${DEFECTDOJO_SONARQUBE_PROJECT_KEY}" \
    --application="${DEFECTDOJO_SONARQUBE_APPLICATION}" \
    --branch="${CI_COMMIT_REF_NAME}" \
    --sonartoken="${SONAR_TOKEN}" \
    --sinceleakperiod="${DEFECTDOJO_SONARQUBE_SINCELEAKPERIOD}" \
    --noSecurityHotspot="${DEFECTDOJO_SONARQUBE_NOSECURITYHOTSPOT}" \
    --allbugs="${DEFECTDOJO_SONARQUBE_ALLBUGS}" > "$sonar_report"
```

## Manual actions during pipeline

Since some security tools (Bandit, Safety...) must be launched manually in the pipeline, proceed in two steps: 
- click manually on the corresponding jobs
- when they are all finished, click on defectdojo-publish job to launch it manually

## Security considerations

Every DefectDojo admin is authorized to access all data.

The current instance of DefectDojo is deployed on the Zener K8S cluster, so every Zener admin can technically access all data related to DefectDojo.

The data stored in the database is not encrypted at rest. Intra-platform network flows (including flows towards the database) are not encrypted (but external traffic is encrypted). There is no network segmentation.


## Version history

See [Tags page](https://gitlab.com/to-be-continuous/defectdojo/-/tags)

## Variants

The DefectDojo template can be used in conjunction with template variants to cover specific cases.

### Vault variant

This variant allows delegating your secrets management to a [Vault](https://www.vaultproject.io/) server.

#### Configuration

In order to be able to communicate with the Vault server, the variant requires the additional configuration parameters:

| Name              | Description                            | Default value     |
| ----------------- | -------------------------------------- | ----------------- |
| `TBC_VAULT_IMAGE` | The [Vault Secrets Provider](https://gitlab.com/to-be-continuous/tools/vault-secrets-provider) image to use (can be overridden) | `$CI_REGISTRY/to-be-continuous/tools/vault-secrets-provider:master` |
| `VAULT_BASE_URL`  | The Vault server base API url          | _none_ |
| `VAULT_OIDC_AUD`  | The `aud` claim for the JWT | `$CI_SERVER_URL` |
| :lock: `VAULT_ROLE_ID`   | The [AppRole](https://www.vaultproject.io/docs/auth/approle) RoleID | **must be defined** |
| :lock: `VAULT_SECRET_ID` | The [AppRole](https://www.vaultproject.io/docs/auth/approle) SecretID | **must be defined** |

#### Usage

Then you may retrieve any of your secret(s) from Vault using the following syntax:

```text
@url@http://vault-secrets-provider/api/secrets/{secret_path}?field={field}
```

With:

| Name                             | Description                            |
| -------------------------------- | -------------------------------------- |
| `secret_path` (_path parameter_) | this is your secret location in the Vault server |
| `field` (_query parameter_)      | parameter to access a single basic field from the secret JSON payload |

#### Example

```yaml
include:
  # main template
  - project: 'to-be-continuous/defectdojo'
    ref: '2.3.2'
    file: '/templates/gitlab-ci-defectdojo.yml'
  # Vault variant
  - project: 'to-be-continuous/defectdojo'
    ref: '2.3.2'
    file: '/templates/gitlab-ci-defectdojo-vault.yml'

variables:
    # audience claim for JWT
    VAULT_OIDC_AUD: "https://vault.acme.host"
    # Secrets managed by Vault
    DEFECTDOJO_API_KEY: "@url@http://vault-secrets-provider/api/secrets/b7ecb6ebabc231/defectdojo/credentials?field=token"
    VAULT_BASE_URL: "https://vault.acme.host/v1"
    # $VAULT_ROLE_ID and $VAULT_SECRET_ID defined as a secret CI/CD variable
```
